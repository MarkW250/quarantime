﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cough : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Called when main cough expansion is finished
    public void CoughFinished()
    {
        //Destroy(this.transform.parent.gameObject);
    }

    // Called after cough circle has faded
    public void CoughFadeFinished()
    {
        Destroy(this.transform.parent.gameObject);
    }


}
