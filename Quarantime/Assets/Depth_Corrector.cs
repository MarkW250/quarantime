﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Depth_Corrector : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 newPosition = new Vector3(transform.position.x, transform.position.y, 1.0f * transform.position.y - 0.5f * transform.position.x);
        transform.position = newPosition;
    }
}
