﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GermCollector : MonoBehaviour
{
    private Player _myPlayer;

    // Start is called before the first frame update
    void Start()
    {
        _myPlayer = GetComponentInParent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "Cough"))
        {
            _myPlayer.CoughingZoneEnter(collision.gameObject.GetComponent<Cough>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if ((collision.gameObject.tag == "Cough"))
        {
            _myPlayer.CoughingZoneExit(collision.gameObject.GetComponent<Cough>());
        }
    }

}
