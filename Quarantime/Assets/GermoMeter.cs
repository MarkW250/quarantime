﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GermoMeter : MonoBehaviour
{
    private SpriteRenderer _myBarFiller;

    private bool _inited = false;

    // Start is called before the first frame update
    void Start()
    {
        Initialise();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetGermPercentage(float germPercentage)
    {
        if(_myBarFiller == null)
        {
            Initialise();
        }
        Vector2 newScale = new Vector2(germPercentage, _myBarFiller.transform.parent.localScale.y);
        _myBarFiller.transform.parent.localScale = newScale;
    }

    private void Initialise()
    {
        if(_inited == false)
        {
            _myBarFiller = null;
            foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>())
            {
                if (sprite.gameObject.name == "BarFiller")
                {
                    _myBarFiller = sprite;
                }
            }

            if (_myBarFiller == null)
            {
                Debug.LogError(gameObject.name + " does not have a bar filler sprite :(");
            }
            _inited = true;
        }
    }
}
