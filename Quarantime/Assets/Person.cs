﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Person : MonoBehaviour
{
    // Public Properties
    [Tooltip("How many seconds between coughs for this person")]
    public float CoughPeriod = 1.0f;

    [Tooltip("Walking speed of this person")]
    public float WalkingSpeed = 2.5f;

    [Tooltip("Prefab to spawn for game object")]
    public GameObject CoughGameObject;

    [Tooltip("List of waypoints to follow")]
    public WayPoint[] WayPoints;

    // My components
    Rigidbody2D _myRigidBody2d;
    AudioSource _myAudioSource;
    PersonBody _myPersonBody;


    // Private state varaibles
    private float _coughTimer = 0.0f;
    private int _wayPointIndex = -1;
    private bool _atWayPoint = false;
    private float _wayPointTimer = 0.0f;
    private Vector2 _currentDestination;


    // Start is called before the first frame update
    void Start()
    {
        // get rigdiy body
        _myRigidBody2d = GetComponent<Rigidbody2D>();
        if (_myRigidBody2d == null)
        {
            Debug.LogError(gameObject.name + " does not have a rigid body 2d!");
        }

        // get rigdiy body
        _myAudioSource = GetComponent<AudioSource>();
        if (_myAudioSource == null)
        {
            Debug.LogError(gameObject.name + " does not have an audio source!");
        }

        // get person body
        _myPersonBody = GetComponentInChildren<PersonBody>();
        if (_myPersonBody == null)
        {
            Debug.LogError("I don't have a PersonBody!");
        }

        if (WayPoints.Length > 0)
        {
            // start with first waypoint
            _wayPointIndex = 0;
            _currentDestination = WayPoints[_wayPointIndex].transform.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // check if this guy should cough
        _coughTimer += Time.deltaTime;
        if (_coughTimer >= CoughPeriod)
        {
            _coughTimer = 0.0f;
            Cough();
        }


        // manage way finding
        if (_wayPointIndex >= 0)
        {
            if (_atWayPoint)
            {
                // wait for current way point timer to expire
                _wayPointTimer -= Time.deltaTime;
                if (_wayPointTimer < 0.0f)
                {
                    // once expired, set next destination to next waypoint
                    _wayPointIndex++;
                    if (_wayPointIndex >= WayPoints.Length)
                    {
                        // TODO for now we cycle through way points, but we should make it an option to end, or to go back in reverse direction
                        _wayPointIndex = 0;
                    }
                    _currentDestination = WayPoints[_wayPointIndex].transform.position;
                    _atWayPoint = false;
                }
            }
            else
            {
                Vector2 destinationVector = _currentDestination - (Vector2)transform.position;
                Vector2 newVelocity = new Vector2(0, 0);
                // check if we have reached our destination
                if (destinationVector.magnitude < 0.1)
                {
                    _atWayPoint = true;
                    _wayPointTimer = WayPoints[_wayPointIndex].PauseTime;
                    if (WayPoints[_wayPointIndex].DirectionToFace != PersonBody.Direction.None)
                    {
                        _myPersonBody.CurrentDirection = WayPoints[_wayPointIndex].DirectionToFace;
                    }
                }
                else
                {
                    // go towards current destination
                    Vector2 directionToDestination = destinationVector.normalized;
                    newVelocity = directionToDestination * WalkingSpeed;
                }

                _myRigidBody2d.velocity = newVelocity;
            }
        }

        // set person body direction according to veloctiy
        setPersonBodyDirection();

    }


    private void Cough()
    {
        GameObject coughObject = Instantiate(CoughGameObject, getCurrentCoughSource().transform.position, getCoughRotation());
        _myAudioSource.Play();
    }

    private Quaternion getCoughRotation()
    {
        Quaternion returnQuat = new Quaternion();
        Vector3 angles = Vector3.zero;

        switch (_myPersonBody.CurrentDirection)
        {
            case PersonBody.Direction.East:
                angles.z = 0.0f;
                break;

            case PersonBody.Direction.NorthEast:
                angles.z = 45.0f;
                break;

            case PersonBody.Direction.North:
                angles.z = 90.0f;
                break;

            case PersonBody.Direction.NorthWest:
                angles.z = 135.0f;
                break;

            case PersonBody.Direction.West:
                angles.z = 180.0f;
                break;

            case PersonBody.Direction.SouthWest:
                angles.z = -135.0f;
                break;

            case PersonBody.Direction.South:
                angles.z = -90.0f;
                break;

            case PersonBody.Direction.SouthEast:
                angles.z = -45.0f;
                break;

            default:
                break;
        }

        returnQuat.eulerAngles = angles;

        return returnQuat;
    }

    private GameObject getCurrentCoughSource()
    {
        GameObject returnObject = null;

        foreach (Transform childTrans in GetComponentsInChildren<Transform>())
        {
            if (childTrans.gameObject.name == "CoughSource")
            {
                returnObject = childTrans.gameObject;
            }
        }


        if (returnObject == null)
        {
            Debug.LogError("I don't have a cough source!");
        }
        return returnObject;
    }

    private void setPersonBodyDirection()
    {
        Vector2 currentVelocity = _myRigidBody2d.velocity;
        float horizontalSpeed = currentVelocity.x;
        float verticalSpeed = currentVelocity.y;
        float minimumSpeed = 0.5f;

        if ((horizontalSpeed > minimumSpeed) && (verticalSpeed > minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthEast;
        }
        else if ((horizontalSpeed < -minimumSpeed) && (verticalSpeed > minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthWest;
        }
        else if ((horizontalSpeed > minimumSpeed) && (verticalSpeed < -minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthEast;
        }
        else if ((horizontalSpeed < -minimumSpeed) && (verticalSpeed < -minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthWest;
        }
        else if (horizontalSpeed > minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.East;
        }
        else if (horizontalSpeed < -minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.West;
        }
        else if (verticalSpeed > minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.North;
        }
        else if (verticalSpeed < -minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.South;
        }
    }

}
