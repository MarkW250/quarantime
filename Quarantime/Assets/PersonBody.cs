﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonBody : MonoBehaviour
{
    // **** Public types ****
 
    // Direction enumerator
    public enum Direction
    {
        None,
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest
    }

    // **** Public Properties ****
    public string DefaultBodyObjectName = "BodyEast";


    // **** Private types ****

    // struct for mapping direction to which gameobject should be enabled when going in that direction
    struct DirectionBodyMappingT
    {
        public DirectionBodyMappingT(Direction dir, string bodyName, float x_scale)
        {
            Direction = dir;
            BodyObjectName = bodyName;
            xScale = x_scale;
        }

        public Direction Direction;
        public string BodyObjectName;
        public float xScale;
    }



    // **** Private Properties ****

    // Mapping table for direction to gameobject name that should be enabled
    private DirectionBodyMappingT[] _directionBodyMappings = new DirectionBodyMappingT[]
    {
        new DirectionBodyMappingT( Direction.East,      "BodyEast",     1.0f),
        new DirectionBodyMappingT( Direction.West,      "BodyEast",    -1.0f),
        new DirectionBodyMappingT( Direction.North,     "BodyNorth",    1.0f)
    };

    // **** My objects ****
    private Rigidbody2D _myRigidBody;

    // **** Private state variables ****
    private GameObject _currentlyActiveGameObject = null;
    private Direction _currentDirection;
    private bool _isWalking;
    


    // **** Public Methods ****
    public Direction CurrentDirection
    {
        get
        {
            return _currentDirection;
        }
        set
        {
            setNewDir(value);
        }
    }

    public bool IsWalking
    {
        get
        {
            return _isWalking;
        }
        set
        {
            _isWalking = value;
            handleAnimationState();
        }
    }


    // **** Private Methods - Unity Engine ****
    // Start is called before the first frame update
    void Start()
    {
        // Get rigid body of parent
        _myRigidBody = null;
        _myRigidBody = GetComponentInParent<Rigidbody2D>();
        if(_myRigidBody == null)
        {
            Debug.LogError(transform.gameObject.name + " cannot find its parent's rigid body!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        // get current velocity and set direction appropriately
        // Vector2 myVelocity = _myRigidBody.velocity;

    }


    // **** Private Methods ****
    private void setNewDir(Direction newDir)
    {
        if (_currentDirection != newDir)
        {
            float scaleModifier = 1.0f;

            _currentDirection = newDir;

            // de-activate current object
            if (_currentlyActiveGameObject != null)
            {
                _currentlyActiveGameObject.SetActive(false);
            }

            _currentlyActiveGameObject = null;
            // look for new object to set active
            foreach (DirectionBodyMappingT mapping in _directionBodyMappings)
            {
                if (mapping.Direction == _currentDirection)
                {
                    _currentlyActiveGameObject = transform.Find(mapping.BodyObjectName).gameObject;
                    scaleModifier = mapping.xScale;
                }
            }

            if (_currentlyActiveGameObject == null)
            {
                Debug.LogError("Could not find body object for direction " + newDir.ToString());
                _currentlyActiveGameObject = transform.Find(DefaultBodyObjectName).gameObject;
            }
            else
            {
                Vector2 currentScale = _currentlyActiveGameObject.transform.localScale;
                currentScale.x = Mathf.Abs(currentScale.x) * scaleModifier;
                _currentlyActiveGameObject.transform.localScale = currentScale;
            }

            _currentlyActiveGameObject.SetActive(true);
        }
    }

    void handleAnimationState()
    {
        Animator myAnimator = GetComponentInChildren<Animator>();
        if(myAnimator != null)
        {
            myAnimator.SetBool("Walking", _isWalking);
        }
    }




}
