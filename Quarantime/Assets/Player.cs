﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    // Public Properties
    [Tooltip("Walking speed of player")]
    public float PlayerSpeed = 5.0f;
    [Tooltip("Maximum germ level of player")]
    public float MaxGermLevel = 100.0f;
    [Tooltip("Time player has to finish level (in seconds)")]
    public float LevelTime = 65.0f;
    [Tooltip("Shopping items that the player needs to collect")]
    public ShoppingItem[] ShoppingItemsToCollect;

    public GermoMeter Ui_GermoMeter;
    public Text Ui_TimerText;

    // My Objects
    private Rigidbody2D _myRigidBody;
    private PersonBody _myPersonBody;

    // Private state variables
    private float _germLevel = 0.0f;
    private List<Cough> _currentCaughs = new List<Cough>();
    private float _coughTimer = 0.0f;
    private float _levelTimer = 0.0f;
    private bool _pickupPressed = false;


    // Start is called before the first frame update
    void Start()
    {
        _myRigidBody = null;

        _myRigidBody = GetComponent<Rigidbody2D>();
        if(_myRigidBody == null)
        {
            Debug.LogError("I don't have a Rigidbody2D!");
        }

        _myPersonBody = GetComponentInChildren<PersonBody>();
        if (_myPersonBody == null)
        {
            Debug.LogError("I don't have a PersonBody!");
        }

        if (Ui_GermoMeter == null)
        {
            Debug.LogError("I don't have a germo-meter assigned!");
        }

        // Player's shopping list items must be set as icons
        foreach(ShoppingItem item in ShoppingItemsToCollect)
        {
            item.IsIcon = true;
        }

        // setup germ o meter by 'adjusting' it
        adjustGermAmount(0);

        // Set level timer
        setRemainingTime(LevelTime);
    }

    // Update is called once per frame
    void Update()
    {
        // Get input and set speed
        float horizontalInput = Input.GetAxisRaw("Horizontal");
        float verticalInput = Input.GetAxisRaw("Vertical");
        Vector2 newSpeed = new Vector2(horizontalInput * PlayerSpeed, verticalInput * PlayerSpeed);
        _myRigidBody.velocity = newSpeed;

        // Update direction of body and animation
        updatePersonBodyDir(horizontalInput, verticalInput);
        if((horizontalInput != 0) || (verticalInput != 0))
        {
            _myPersonBody.IsWalking = true;
        }
        else
        {
            _myPersonBody.IsWalking = false;
        }

        // check to pickup
        if (_pickupPressed == false)
        {
            if (Input.GetAxis("PickUp") > Mathf.Epsilon)
            {
                _pickupPressed = true;
                onPickUpAction();
            }
        }
        else
        {
            if (Input.GetAxis("PickUp") <= Mathf.Epsilon)
            {
                _pickupPressed = false;
            }
        }

        // Take Damage for any cough clouds
        handleDamage();

        // subtract time left
        setRemainingTime(_levelTimer - Time.deltaTime);
    }


    private void updatePersonBodyDir(float horizontalInput, float verticalInput)
    {
        if((horizontalInput > 0) && (verticalInput > 0))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthEast;
        }
        else if ((horizontalInput < 0) && (verticalInput > 0))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthWest;
        }
        else if ((horizontalInput > 0) && (verticalInput < 0))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthEast;
        }
        else if ((horizontalInput < 0) && (verticalInput < 0))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthWest;
        }
        else if (horizontalInput > 0)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.East;
        }
        else if (horizontalInput < 0)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.West;
        }
        else if (verticalInput > 0)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.North;
        }
        else if (verticalInput < 0)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.South;
        }
    }


    private void adjustGermAmount(float adjust)
    {
        _germLevel += adjust;

        // cannot go lower than 0
        if(_germLevel < 0)
        {
            _germLevel = 0;
        }

        // cannot go greater than max level
        if(_germLevel >= MaxGermLevel)
        {
            _germLevel = MaxGermLevel;

            // TODO: GameOver!
        }

        Ui_GermoMeter.SetGermPercentage(_germLevel / MaxGermLevel);
    }


    private void handleDamage()
    {
        _coughTimer += Time.deltaTime;
        // take damage for every 10 milliseconds you are in the cough cloud
        if (_coughTimer >= 0.01f)
        {
            _coughTimer = 0.0f;
            foreach (Cough cough in _currentCaughs)
            {
                adjustGermAmount(0.1f);
            }
        }
    }

    private void setRemainingTime(float remainingTime)
    {
        _levelTimer = remainingTime;
        if(_levelTimer < 0.0f)
        {
            _levelTimer = 0.0f;
            TimeOver();
        }

        int minutesLeft = (int)(_levelTimer / 60.0f);
        int secondsLeft = (int)(_levelTimer) - minutesLeft * 60;
        string timeString = string.Format("{0:D2}:{1:D2}", minutesLeft, secondsLeft);
        Ui_TimerText.text = timeString;
        if (_levelTimer < 15.0f)
        {
            Color newColor = new Color();
            newColor.a = 1;
            newColor.r = 1;
            newColor.g = 0;
            newColor.b = 0;
            Ui_TimerText.color = newColor;
        }
    }


    private void TimeOver()
    {
        Debug.Log("TIME OVER!");
        // TODO
        
        // show fail screen
    }

    private Collider2D getCurrentPickUpZone()
    {
        Collider2D returnCollider = null;
        // get pickup zone collider
        foreach (Collider2D collider in GetComponentsInChildren<Collider2D>())
        {
            if (collider.gameObject.name == "PickupZone")
            {
                returnCollider = collider;
            }
        }
        if (returnCollider == null)
        {
            Debug.LogError("I don't have a PickUp zone collider!");
        }
        return returnCollider;
    }

    private void onPickUpAction()
    {
        Collider2D currentPickUpZone = getCurrentPickUpZone();
        List<Collider2D> colliders = new List<Collider2D>();
        // check if our pickup zone is overlapping a shopping item
        ContactFilter2D filter = new ContactFilter2D();
        filter.useTriggers = true;
        currentPickUpZone.OverlapCollider(filter, colliders);
        foreach(Collider2D collider in colliders)
        {
            if(collider.gameObject.tag == "ShoppingItem")
            {
                ShoppingItem itemPickedUp = collider.gameObject.GetComponent<ShoppingItem>();
                Debug.Log("I have picked up a " + itemPickedUp.ItemType + "!");
                checkItemTickOff(itemPickedUp);

                // mark picked up item as collected
                itemPickedUp.Collected = true;
            }
        }
    }

    private void checkItemTickOff(ShoppingItem itemToCheck)
    {
        bool allItemsCollected = true;
        foreach(ShoppingItem item in ShoppingItemsToCollect)
        {
            if(item.ItemType == itemToCheck.ItemType)
            {
                item.Collected = true;
            }

            if(item.Collected == false)
            {
                allItemsCollected = false;
            }
        }

        if(allItemsCollected)
        {
            // TODO
            // Here we have collected all items. We can play a noise or indicate to the player that he can go back to the counter or something
            Debug.Log("I have finished my shopping!");
        }
    }


    // Public methods

    // To be called by germ detector
    public void CoughingZoneEnter(Cough cougher)
    {
        if (cougher != null)
        {
            _currentCaughs.Add(cougher);
            Debug.Log("I am being coughed on by " + cougher.name);
        }
    }

    // To be called by germ detector
    public void CoughingZoneExit(Cough cougher)
    {
        _currentCaughs.Remove(cougher);
    }

}
