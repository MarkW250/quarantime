﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoppingItem : MonoBehaviour
{
    [Tooltip("Type of item (bread, milk etc.)")]
    public string ItemType;

    [Tooltip("Inidcates if this is used as an icon for displaying the player's shopping list")]
    public bool IsIcon = false;


    private bool _collectedB = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool Collected
    {
        get
        {
            return _collectedB;
        }
        set
        {
            if(_collectedB == false)
            {
                _collectedB = true;
                onCollected();
            }
        }
    }

    private void onCollected()
    {
        if(IsIcon)
        {
            // TODO
            // Here, we should play animation for icon (that puts a tick or something on the icon
        }
        else
        {
            // TODO
            // Here, we should play an animation that makes item somehow dissappear into the basket
            // for now, just destroy it immediately
            OnAnimation_CollectionFinished();
        }
    }

    public void OnAnimation_CollectionFinished()
    {
        Destroy(this.gameObject);
    }
}
