﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour
{

    /**** Public Properties ****/
    [Tooltip("Time that shopper person should wait at this waypoint")]
    public float PauseTime;

    [Tooltip("Direction the shopper person should have when stopping at this waypoint")]
    public PersonBody.Direction DirectionToFace;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
