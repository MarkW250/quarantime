# README #

This is intended to be a game entry for DevTV Game Jam (https://itch.io/jam/gamedevtv-community-jam)


## NOTE: Sounds

Royalty-free sound effects for coughing were used that I will not upload to the repository. They are available from: https://www.fesliyanstudios.com
Please give them a visit!

To load the project completely with sound assets, please download the following sound effects from the above website and place them in the 'Sounds' folder:
Small-Double-Cough-3

Background music is also royalty free from here: https://www.bensound.com/royalty-free-music/track/the-elevator-bossa-nova
The music in that link also needs to be put into the sounds folder.
